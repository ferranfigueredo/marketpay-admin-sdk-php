<?php
/**
 * TpgUniversalPayConfigsApi
 * PHP version 5
 *
 * @category Class
 * @package  MarketPayAdmin
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * MarketPay API
 *
 * API for Smart Contracts and Payments
 *
 * OpenAPI spec version: admin
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace MarketPayAdmin\Api;

use \MarketPayAdmin\ApiClient;
use \MarketPayAdmin\ApiException;
use \MarketPayAdmin\Configuration;
use \MarketPayAdmin\ObjectSerializer;

/**
 * TpgUniversalPayConfigsApi Class Doc Comment
 *
 * @category Class
 * @package  MarketPayAdmin
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class TpgUniversalPayConfigsApi
{
    /**
     * API Client
     *
     * @var \MarketPayAdmin\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \MarketPayAdmin\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\MarketPayAdmin\ApiClient $apiClient = null)
    {
        if ($apiClient === null) {
            $apiClient = new ApiClient();
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \MarketPayAdmin\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \MarketPayAdmin\ApiClient $apiClient set the API client
     *
     * @return TpgUniversalPayConfigsApi
     */
    public function setApiClient(\MarketPayAdmin\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation tpgUniversalPayConfigsGet
     *
     * @param int $id  (required)
     * @throws \MarketPayAdmin\ApiException on non-2xx response
     * @return \MarketPayAdmin\Model\TpgUniversalPayConfigResponse
     */
    public function tpgUniversalPayConfigsGet($id)
    {
        list($response) = $this->tpgUniversalPayConfigsGetWithHttpInfo($id);
        return $response;
    }

    /**
     * Operation tpgUniversalPayConfigsGetWithHttpInfo
     *
     * @param int $id  (required)
     * @throws \MarketPayAdmin\ApiException on non-2xx response
     * @return array of \MarketPayAdmin\Model\TpgUniversalPayConfigResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function tpgUniversalPayConfigsGetWithHttpInfo($id)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling tpgUniversalPayConfigsGet');
        }
        // parse inputs
        $resourcePath = "/v2.01/core/TpgUniversalPayConfigs/{id}";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['text/plain', 'application/json', 'text/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType([]);

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\MarketPayAdmin\Model\TpgUniversalPayConfigResponse',
                '/v2.01/core/TpgUniversalPayConfigs/{id}'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\MarketPayAdmin\Model\TpgUniversalPayConfigResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\MarketPayAdmin\Model\TpgUniversalPayConfigResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\MarketPayAdmin\Model\CustomApiErrorResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation tpgUniversalPayConfigsPost
     *
     * @param \MarketPayAdmin\Model\TpgUniversalPayConfigPost $request  (optional)
     * @throws \MarketPayAdmin\ApiException on non-2xx response
     * @return \MarketPayAdmin\Model\TpgUniversalPayConfigResponse
     */
    public function tpgUniversalPayConfigsPost($request = null)
    {
        list($response) = $this->tpgUniversalPayConfigsPostWithHttpInfo($request);
        return $response;
    }

    /**
     * Operation tpgUniversalPayConfigsPostWithHttpInfo
     *
     * @param \MarketPayAdmin\Model\TpgUniversalPayConfigPost $request  (optional)
     * @throws \MarketPayAdmin\ApiException on non-2xx response
     * @return array of \MarketPayAdmin\Model\TpgUniversalPayConfigResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function tpgUniversalPayConfigsPostWithHttpInfo($request = null)
    {
        // parse inputs
        $resourcePath = "/v2.01/core/TpgUniversalPayConfigs";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['text/plain', 'application/json', 'text/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json']);

        // body params
        $_tempBody = null;
        if (isset($request)) {
            $_tempBody = $request;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\MarketPayAdmin\Model\TpgUniversalPayConfigResponse',
                '/v2.01/core/TpgUniversalPayConfigs'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\MarketPayAdmin\Model\TpgUniversalPayConfigResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\MarketPayAdmin\Model\TpgUniversalPayConfigResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
                case 400:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\MarketPayAdmin\Model\CustomApiErrorResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }
}
