<?php
/**
 * CardsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  MarketPayAdmin
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * MarketPay API
 *
 * API for Smart Contracts and Payments
 *
 * OpenAPI spec version: admin
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace MarketPayAdmin;

use \MarketPayAdmin\Configuration;
use \MarketPayAdmin\ApiClient;
use \MarketPayAdmin\ApiException;
use \MarketPayAdmin\ObjectSerializer;

/**
 * CardsApiTest Class Doc Comment
 *
 * @category Class
 * @package  MarketPayAdmin
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CardsApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for cardsGetClientList
     *
     * Clients List of Card.
     *
     */
    public function testCardsGetClientList()
    {
    }
}
