<?php
/**
 * AplazameAccountTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  MarketPayAdmin
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * MarketPay API
 *
 * API for Smart Contracts and Payments
 *
 * OpenAPI spec version: admin
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace MarketPayAdmin;

/**
 * AplazameAccountTest Class Doc Comment
 *
 * @category    Class */
// * @description AplazameAccount
/**
 * @package     MarketPayAdmin
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AplazameAccountTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AplazameAccount"
     */
    public function testAplazameAccount()
    {
    }

    /**
     * Test attribute "environment"
     */
    public function testPropertyEnvironment()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "public_key"
     */
    public function testPropertyPublicKey()
    {
    }

    /**
     * Test attribute "secret_key"
     */
    public function testPropertySecretKey()
    {
    }

    /**
     * Test attribute "last_order"
     */
    public function testPropertyLastOrder()
    {
    }
}
