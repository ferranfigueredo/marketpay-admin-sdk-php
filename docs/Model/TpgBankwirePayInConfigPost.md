# TpgBankwirePayInConfigPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**owner_name** | **string** |  | [optional] 
**owner_address** | [**\MarketPayAdmin\Model\Address**](Address.md) |  | [optional] 
**ca_bank_name** | **string** |  | [optional] 
**ca_institution_number** | **string** |  | [optional] 
**ca_branch_code** | **string** |  | [optional] 
**ca_account_number** | **string** |  | [optional] 
**gb_account_number** | **string** |  | [optional] 
**gb_sort_code** | **string** |  | [optional] 
**iban_iban** | **string** |  | [optional] 
**iban_bic** | **string** |  | [optional] 
**other_country** | **string** |  | [optional] 
**other_bic** | **string** |  | [optional] 
**other_account_number** | **string** |  | [optional] 
**us_account_number** | **string** |  | [optional] 
**us_aba** | **string** |  | [optional] 
**us_deposit_account_type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


