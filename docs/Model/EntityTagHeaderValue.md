# EntityTagHeaderValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | [**\MarketPayAdmin\Model\StringSegment**](StringSegment.md) |  | [optional] 
**is_weak** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


