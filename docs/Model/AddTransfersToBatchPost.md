# AddTransfersToBatchPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transfer_list** | **int[]** |  | [optional] 
**client_id** | **string** |  | [optional] 
**date_start** | **int** |  | [optional] 
**date_end** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


