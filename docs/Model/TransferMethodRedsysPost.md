# TransferMethodRedsysPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **string** |  | [optional] 
**account_id** | **string** |  | [optional] 
**account** | [**\MarketPayAdmin\Model\RedsysAccount**](RedsysAccount.md) |  | [optional] 
**currency** | **string** |  | [optional] 
**terminal** | **string** |  | [optional] 
**redsys_environment** | **string** |  | [optional] 
**merchant_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


