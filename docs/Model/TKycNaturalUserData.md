# TKycNaturalUserData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**first_name** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**last_name** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**address** | [**\MarketPayAdmin\Model\TAddressValidationResult**](TAddressValidationResult.md) |  | [optional] 
**telephone** | [**\MarketPayAdmin\Model\TTelephoneValidationResult**](TTelephoneValidationResult.md) |  | [optional] 
**birthday** | [**\MarketPayAdmin\Model\PropertyValidationDateNullable**](PropertyValidationDateNullable.md) |  | [optional] 
**nationality** | [**\MarketPayAdmin\Model\PropertyValidationCountry**](PropertyValidationCountry.md) |  | [optional] 
**country_of_residence** | [**\MarketPayAdmin\Model\PropertyValidationCountry**](PropertyValidationCountry.md) |  | [optional] 
**occupation** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**id_card** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**id_card_document** | [**\MarketPayAdmin\Model\TKycFileDetails**](TKycFileDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


