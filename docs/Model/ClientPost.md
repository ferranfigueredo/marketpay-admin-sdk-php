# ClientPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**security_identifier** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**document** | **string** |  | [optional] 
**contact** | **string** |  | [optional] 
**contact_email** | **string** |  | [optional] 
**terms_conditions** | **bool** |  | [optional] 
**kyc_check** | **bool** |  | [optional] 
**service_agree** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


