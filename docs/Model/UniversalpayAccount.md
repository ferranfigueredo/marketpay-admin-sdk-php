# UniversalpayAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**environment** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**public_key** | **string** |  | [optional] 
**secret_key** | **string** |  | [optional] 
**last_order** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


