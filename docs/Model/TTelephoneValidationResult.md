# TTelephoneValidationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country_code** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**number** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


