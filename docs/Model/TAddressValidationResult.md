# TAddressValidationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_line1** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**address_line2** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**city** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**region** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**postal_code** | [**\MarketPayAdmin\Model\PropertyValidationString**](PropertyValidationString.md) |  | [optional] 
**country** | [**\MarketPayAdmin\Model\PropertyValidationCountry**](PropertyValidationCountry.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


