# FileResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type** | **string** |  | [optional] 
**file_download_name** | **string** |  | [optional] 
**last_modified** | [**\DateTime**](\DateTime.md) |  | [optional] 
**entity_tag** | [**\MarketPayAdmin\Model\EntityTagHeaderValue**](EntityTagHeaderValue.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


