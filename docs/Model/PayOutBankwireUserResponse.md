# PayOutBankwireUserResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**person_type** | **string** |  | [optional] 
**kyc_level** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**legal_name** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**address** | [**\MarketPayAdmin\Model\Address**](Address.md) |  | [optional] 
**id** | **string** | The item&#39;s ID | [optional] 
**creation_date** | **int** | When the item was created | [optional] 
**tag** | **string** | Custom data that you can add to this item | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


