# PayOutBankwireCoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author_id** | **string** |  | [optional] 
**credited_user_id** | **string** |  | [optional] 
**debited_funds** | [**\MarketPayAdmin\Model\Money**](Money.md) |  | [optional] 
**credited_funds** | [**\MarketPayAdmin\Model\Money**](Money.md) |  | [optional] 
**fees** | [**\MarketPayAdmin\Model\Money**](Money.md) |  | [optional] 
**status** | **string** |  | [optional] 
**result_code** | **string** |  | [optional] 
**result_message** | **string** |  | [optional] 
**execution_date** | **int** |  | [optional] 
**type** | **string** |  | [optional] 
**nature** | **string** |  | [optional] 
**debited_wallet_id** | **string** |  | [optional] 
**credited_wallet_id** | **string** |  | [optional] 
**payment_type** | **string** |  | [optional] 
**bank_wire_ref** | **string** |  | [optional] 
**user_response** | [**\MarketPayAdmin\Model\PayOutBankwireUserResponse**](PayOutBankwireUserResponse.md) |  | [optional] 
**bank_account** | [**\MarketPayAdmin\Model\BankAccountResponseAsChild**](BankAccountResponseAsChild.md) |  | [optional] 
**id** | **string** | The item&#39;s ID | [optional] 
**creation_date** | **int** | When the item was created | [optional] 
**tag** | **string** | Custom data that you can add to this item | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


