# BankAccountType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | The type of bank account | [optional] 
**owner_address** | [**\MarketPayAdmin\Model\Address**](Address.md) | The address of the owner of the bank account | [optional] 
**owner_name** | **string** | The name of the owner of the bank account | [optional] 
**iban** | **string** | The IBAN of the bank account | [optional] 
**bic** | **string** | The BIC of the bank account | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


