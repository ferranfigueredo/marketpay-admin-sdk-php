# TpsSeurConfigPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**environment** | **string** |  | [optional] 
**key** | **string** |  | [optional] 
**secret** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


