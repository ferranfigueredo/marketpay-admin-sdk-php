# ResponseListKycUserPaginatedItemResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**\MarketPayAdmin\Model\KycUserPaginatedItemResponse[]**](KycUserPaginatedItemResponse.md) |  | [optional] 
**total** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


