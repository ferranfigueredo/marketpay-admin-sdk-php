# StringSegment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buffer** | **string** |  | [optional] 
**offset** | **int** |  | [optional] 
**length** | **int** |  | [optional] 
**value** | **string** |  | [optional] 
**has_value** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


