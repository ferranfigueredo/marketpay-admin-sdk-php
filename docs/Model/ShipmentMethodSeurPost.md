# ShipmentMethodSeurPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **string** |  | [optional] 
**account_id** | **string** |  | [optional] 
**account** | [**\MarketPayAdmin\Model\SeurAccount**](SeurAccount.md) |  | [optional] 
**ccc_ordenante** | **string** |  | [optional] 
**tax_id_number** | **string** |  | [optional] 
**tax_id_country** | **string** |  | [optional] 
**address** | [**\MarketPayAdmin\Model\Address**](Address.md) |  | [optional] 
**company_name** | **string** |  | [optional] 
**business_name** | **string** |  | [optional] 
**contact_first_name** | **string** |  | [optional] 
**contact_last_name** | **string** |  | [optional] 
**telephone** | [**\MarketPayAdmin\Model\Telephone**](Telephone.md) |  | [optional] 
**fax** | [**\MarketPayAdmin\Model\Telephone**](Telephone.md) |  | [optional] 
**email** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


