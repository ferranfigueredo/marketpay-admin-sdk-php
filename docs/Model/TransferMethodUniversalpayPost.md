# TransferMethodUniversalpayPost

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **string** |  | [optional] 
**account_id** | **string** |  | [optional] 
**account** | [**\MarketPayAdmin\Model\UniversalpayAccount**](UniversalpayAccount.md) |  | [optional] 
**currency** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


