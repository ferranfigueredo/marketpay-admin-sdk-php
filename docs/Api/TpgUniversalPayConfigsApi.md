# MarketPayAdmin\TpgUniversalPayConfigsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tpgUniversalPayConfigsGet**](TpgUniversalPayConfigsApi.md#tpgUniversalPayConfigsGet) | **GET** /v2.01/core/TpgUniversalPayConfigs/{id} | 
[**tpgUniversalPayConfigsPost**](TpgUniversalPayConfigsApi.md#tpgUniversalPayConfigsPost) | **POST** /v2.01/core/TpgUniversalPayConfigs | 


# **tpgUniversalPayConfigsGet**
> \MarketPayAdmin\Model\TpgUniversalPayConfigResponse tpgUniversalPayConfigsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgUniversalPayConfigsApi();
$id = 789; // int | 

try {
    $result = $api_instance->tpgUniversalPayConfigsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgUniversalPayConfigsApi->tpgUniversalPayConfigsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TpgUniversalPayConfigResponse**](../Model/TpgUniversalPayConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tpgUniversalPayConfigsPost**
> \MarketPayAdmin\Model\TpgUniversalPayConfigResponse tpgUniversalPayConfigsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgUniversalPayConfigsApi();
$request = new \MarketPayAdmin\Model\TpgUniversalPayConfigPost(); // \MarketPayAdmin\Model\TpgUniversalPayConfigPost | 

try {
    $result = $api_instance->tpgUniversalPayConfigsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgUniversalPayConfigsApi->tpgUniversalPayConfigsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TpgUniversalPayConfigPost**](../Model/TpgUniversalPayConfigPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TpgUniversalPayConfigResponse**](../Model/TpgUniversalPayConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

