# MarketPayAdmin\TpsSeurConfigsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tpsSeurConfigsGet**](TpsSeurConfigsApi.md#tpsSeurConfigsGet) | **GET** /v2.01/core/TpsSeurConfigs/{id} | 
[**tpsSeurConfigsPost**](TpsSeurConfigsApi.md#tpsSeurConfigsPost) | **POST** /v2.01/core/TpsSeurConfigs | 


# **tpsSeurConfigsGet**
> \MarketPayAdmin\Model\TpsSeurConfigResponse tpsSeurConfigsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpsSeurConfigsApi();
$id = 789; // int | 

try {
    $result = $api_instance->tpsSeurConfigsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpsSeurConfigsApi->tpsSeurConfigsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TpsSeurConfigResponse**](../Model/TpsSeurConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tpsSeurConfigsPost**
> \MarketPayAdmin\Model\TpsSeurConfigResponse tpsSeurConfigsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpsSeurConfigsApi();
$request = new \MarketPayAdmin\Model\TpsSeurConfigPost(); // \MarketPayAdmin\Model\TpsSeurConfigPost | 

try {
    $result = $api_instance->tpsSeurConfigsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpsSeurConfigsApi->tpsSeurConfigsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TpsSeurConfigPost**](../Model/TpsSeurConfigPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TpsSeurConfigResponse**](../Model/TpsSeurConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

