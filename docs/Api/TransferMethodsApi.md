# MarketPayAdmin\TransferMethodsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**transferMethodsAplazameGet**](TransferMethodsApi.md#transferMethodsAplazameGet) | **GET** /v2.01/core/TransferMethods/aplazame/{id} | 
[**transferMethodsAplazameGetList**](TransferMethodsApi.md#transferMethodsAplazameGetList) | **GET** /v2.01/core/TransferMethods/aplazame | 
[**transferMethodsAplazamePost**](TransferMethodsApi.md#transferMethodsAplazamePost) | **POST** /v2.01/core/TransferMethods/aplazame | 
[**transferMethodsBankwirePayInGet**](TransferMethodsApi.md#transferMethodsBankwirePayInGet) | **GET** /v2.01/core/TransferMethods/bankwirePayIn/{id} | 
[**transferMethodsBankwirePayInGetList**](TransferMethodsApi.md#transferMethodsBankwirePayInGetList) | **GET** /v2.01/core/TransferMethods/bankwirePayIn | 
[**transferMethodsBankwirePayInPost**](TransferMethodsApi.md#transferMethodsBankwirePayInPost) | **POST** /v2.01/core/TransferMethods/bankwirePayIn | 
[**transferMethodsBankwirePayOutGet**](TransferMethodsApi.md#transferMethodsBankwirePayOutGet) | **GET** /v2.01/core/TransferMethods/bankwirePayOut/{id} | 
[**transferMethodsBankwirePayOutGetList**](TransferMethodsApi.md#transferMethodsBankwirePayOutGetList) | **GET** /v2.01/core/TransferMethods/bankwirePayOut | 
[**transferMethodsBankwirePayOutPost**](TransferMethodsApi.md#transferMethodsBankwirePayOutPost) | **POST** /v2.01/core/TransferMethods/bankwirePayOut | 
[**transferMethodsRedsysGet**](TransferMethodsApi.md#transferMethodsRedsysGet) | **GET** /v2.01/core/TransferMethods/redsys/{id} | 
[**transferMethodsRedsysGetList**](TransferMethodsApi.md#transferMethodsRedsysGetList) | **GET** /v2.01/core/TransferMethods/redsys | 
[**transferMethodsRedsysPost**](TransferMethodsApi.md#transferMethodsRedsysPost) | **POST** /v2.01/core/TransferMethods/redsys | 
[**transferMethodsUniversalPayGetList**](TransferMethodsApi.md#transferMethodsUniversalPayGetList) | **GET** /v2.01/core/TransferMethods/universalpay | 
[**transferMethodsUniversalpayGet**](TransferMethodsApi.md#transferMethodsUniversalpayGet) | **GET** /v2.01/core/TransferMethods/universalpay/{id} | 
[**transferMethodsUniversalpayPost**](TransferMethodsApi.md#transferMethodsUniversalpayPost) | **POST** /v2.01/core/TransferMethods/universalpay | 


# **transferMethodsAplazameGet**
> \MarketPayAdmin\Model\TransferMethodAplazameResponse transferMethodsAplazameGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$id = 789; // int | 

try {
    $result = $api_instance->transferMethodsAplazameGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsAplazameGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TransferMethodAplazameResponse**](../Model/TransferMethodAplazameResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsAplazameGetList**
> \MarketPayAdmin\Model\ResponseListTransferMethodAplazameResponse transferMethodsAplazameGetList()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();

try {
    $result = $api_instance->transferMethodsAplazameGetList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsAplazameGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\MarketPayAdmin\Model\ResponseListTransferMethodAplazameResponse**](../Model/ResponseListTransferMethodAplazameResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsAplazamePost**
> \MarketPayAdmin\Model\TransferMethodAplazameResponse transferMethodsAplazamePost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$request = new \MarketPayAdmin\Model\TransferMethodAplazamePost(); // \MarketPayAdmin\Model\TransferMethodAplazamePost | 

try {
    $result = $api_instance->transferMethodsAplazamePost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsAplazamePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TransferMethodAplazamePost**](../Model/TransferMethodAplazamePost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TransferMethodAplazameResponse**](../Model/TransferMethodAplazameResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsBankwirePayInGet**
> \MarketPayAdmin\Model\TransferMethodBankwirePayInResponse transferMethodsBankwirePayInGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$id = 789; // int | 

try {
    $result = $api_instance->transferMethodsBankwirePayInGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsBankwirePayInGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TransferMethodBankwirePayInResponse**](../Model/TransferMethodBankwirePayInResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsBankwirePayInGetList**
> \MarketPayAdmin\Model\ResponseListTransferMethodBankwirePayInResponse transferMethodsBankwirePayInGetList()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();

try {
    $result = $api_instance->transferMethodsBankwirePayInGetList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsBankwirePayInGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\MarketPayAdmin\Model\ResponseListTransferMethodBankwirePayInResponse**](../Model/ResponseListTransferMethodBankwirePayInResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsBankwirePayInPost**
> \MarketPayAdmin\Model\TransferMethodBankwirePayInResponse transferMethodsBankwirePayInPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$request = new \MarketPayAdmin\Model\TransferMethodBankwirePayInPost(); // \MarketPayAdmin\Model\TransferMethodBankwirePayInPost | 

try {
    $result = $api_instance->transferMethodsBankwirePayInPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsBankwirePayInPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TransferMethodBankwirePayInPost**](../Model/TransferMethodBankwirePayInPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TransferMethodBankwirePayInResponse**](../Model/TransferMethodBankwirePayInResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsBankwirePayOutGet**
> \MarketPayAdmin\Model\TransferMethodBankwirePayOutResponse transferMethodsBankwirePayOutGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$id = 789; // int | 

try {
    $result = $api_instance->transferMethodsBankwirePayOutGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsBankwirePayOutGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TransferMethodBankwirePayOutResponse**](../Model/TransferMethodBankwirePayOutResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsBankwirePayOutGetList**
> \MarketPayAdmin\Model\ResponseListTransferMethodBankwirePayOutResponse transferMethodsBankwirePayOutGetList()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();

try {
    $result = $api_instance->transferMethodsBankwirePayOutGetList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsBankwirePayOutGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\MarketPayAdmin\Model\ResponseListTransferMethodBankwirePayOutResponse**](../Model/ResponseListTransferMethodBankwirePayOutResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsBankwirePayOutPost**
> \MarketPayAdmin\Model\TransferMethodBankwirePayOutResponse transferMethodsBankwirePayOutPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$request = new \MarketPayAdmin\Model\TransferMethodBankwirePayOutPost(); // \MarketPayAdmin\Model\TransferMethodBankwirePayOutPost | 

try {
    $result = $api_instance->transferMethodsBankwirePayOutPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsBankwirePayOutPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TransferMethodBankwirePayOutPost**](../Model/TransferMethodBankwirePayOutPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TransferMethodBankwirePayOutResponse**](../Model/TransferMethodBankwirePayOutResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsRedsysGet**
> \MarketPayAdmin\Model\TransferMethodRedsysResponse transferMethodsRedsysGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$id = 789; // int | 

try {
    $result = $api_instance->transferMethodsRedsysGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsRedsysGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TransferMethodRedsysResponse**](../Model/TransferMethodRedsysResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsRedsysGetList**
> \MarketPayAdmin\Model\ResponseListTransferMethodRedsysResponse transferMethodsRedsysGetList()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();

try {
    $result = $api_instance->transferMethodsRedsysGetList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsRedsysGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\MarketPayAdmin\Model\ResponseListTransferMethodRedsysResponse**](../Model/ResponseListTransferMethodRedsysResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsRedsysPost**
> \MarketPayAdmin\Model\TransferMethodRedsysResponse transferMethodsRedsysPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$request = new \MarketPayAdmin\Model\TransferMethodRedsysPost(); // \MarketPayAdmin\Model\TransferMethodRedsysPost | 

try {
    $result = $api_instance->transferMethodsRedsysPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsRedsysPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TransferMethodRedsysPost**](../Model/TransferMethodRedsysPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TransferMethodRedsysResponse**](../Model/TransferMethodRedsysResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsUniversalPayGetList**
> \MarketPayAdmin\Model\ResponseListTransferMethodUniversalpayResponse transferMethodsUniversalPayGetList()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();

try {
    $result = $api_instance->transferMethodsUniversalPayGetList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsUniversalPayGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\MarketPayAdmin\Model\ResponseListTransferMethodUniversalpayResponse**](../Model/ResponseListTransferMethodUniversalpayResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsUniversalpayGet**
> \MarketPayAdmin\Model\TransferMethodUniversalpayResponse transferMethodsUniversalpayGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$id = 789; // int | 

try {
    $result = $api_instance->transferMethodsUniversalpayGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsUniversalpayGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TransferMethodUniversalpayResponse**](../Model/TransferMethodUniversalpayResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transferMethodsUniversalpayPost**
> \MarketPayAdmin\Model\TransferMethodUniversalpayResponse transferMethodsUniversalpayPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TransferMethodsApi();
$request = new \MarketPayAdmin\Model\TransferMethodUniversalpayPost(); // \MarketPayAdmin\Model\TransferMethodUniversalpayPost | 

try {
    $result = $api_instance->transferMethodsUniversalpayPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransferMethodsApi->transferMethodsUniversalpayPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TransferMethodUniversalpayPost**](../Model/TransferMethodUniversalpayPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TransferMethodUniversalpayResponse**](../Model/TransferMethodUniversalpayResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

