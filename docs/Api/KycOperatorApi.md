# MarketPayAdmin\KycOperatorApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**kycOperatorGetFile**](KycOperatorApi.md#kycOperatorGetFile) | **GET** /v2.01/core/KycOperator/document/{DocumentId} | 
[**kycOperatorGetNaturaList**](KycOperatorApi.md#kycOperatorGetNaturaList) | **GET** /v2.01/core/KycOperator/users/natural | List all Natural User
[**kycOperatorGetValidationNatural**](KycOperatorApi.md#kycOperatorGetValidationNatural) | **GET** /v2.01/core/KycOperator/validations/natural/{ValidationId} | 
[**kycOperatorGetpendingValidations**](KycOperatorApi.md#kycOperatorGetpendingValidations) | **GET** /v2.01/core/KycOperator/validations/pending | 
[**kycOperatorPutOperatorValidation**](KycOperatorApi.md#kycOperatorPutOperatorValidation) | **PUT** /v2.01/core/KycOperator/validations/natural/{ValidationId} | 


# **kycOperatorGetFile**
> \SplFileObject kycOperatorGetFile($document_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\KycOperatorApi();
$document_id = 789; // int | 

try {
    $result = $api_instance->kycOperatorGetFile($document_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KycOperatorApi->kycOperatorGetFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_id** | **int**|  |

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **kycOperatorGetNaturaList**
> \MarketPayAdmin\Model\ResponseListKycUserValidationLevelNaturalResponse kycOperatorGetNaturaList($client_id, $page, $per_page, $first_name_contains, $last_name_contains, $id_card_contains)

List all Natural User



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\KycOperatorApi();
$client_id = 789; // int | 
$page = 56; // int | The page number of results you wish to return
$per_page = 56; // int | The number of results to return per page
$first_name_contains = "first_name_contains_example"; // string | 
$last_name_contains = "last_name_contains_example"; // string | 
$id_card_contains = "id_card_contains_example"; // string | 

try {
    $result = $api_instance->kycOperatorGetNaturaList($client_id, $page, $per_page, $first_name_contains, $last_name_contains, $id_card_contains);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KycOperatorApi->kycOperatorGetNaturaList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **int**|  | [optional]
 **page** | **int**| The page number of results you wish to return | [optional]
 **per_page** | **int**| The number of results to return per page | [optional]
 **first_name_contains** | **string**|  | [optional]
 **last_name_contains** | **string**|  | [optional]
 **id_card_contains** | **string**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListKycUserValidationLevelNaturalResponse**](../Model/ResponseListKycUserValidationLevelNaturalResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **kycOperatorGetValidationNatural**
> \MarketPayAdmin\Model\TKycNaturalUserData kycOperatorGetValidationNatural($validation_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\KycOperatorApi();
$validation_id = 789; // int | 

try {
    $result = $api_instance->kycOperatorGetValidationNatural($validation_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KycOperatorApi->kycOperatorGetValidationNatural: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **validation_id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TKycNaturalUserData**](../Model/TKycNaturalUserData.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **kycOperatorGetpendingValidations**
> \MarketPayAdmin\Model\ResponseListKycUserPaginatedItemResponse kycOperatorGetpendingValidations($client_id, $page, $per_page)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\KycOperatorApi();
$client_id = 789; // int | 
$page = 56; // int | 
$per_page = 56; // int | 

try {
    $result = $api_instance->kycOperatorGetpendingValidations($client_id, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KycOperatorApi->kycOperatorGetpendingValidations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **int**|  | [optional]
 **page** | **int**|  | [optional]
 **per_page** | **int**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListKycUserPaginatedItemResponse**](../Model/ResponseListKycUserPaginatedItemResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **kycOperatorPutOperatorValidation**
> \MarketPayAdmin\Model\TKycNaturalUserData kycOperatorPutOperatorValidation($validation_id, $operator_result)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\KycOperatorApi();
$validation_id = 789; // int | 
$operator_result = new \MarketPayAdmin\Model\TKycNaturalUserData(); // \MarketPayAdmin\Model\TKycNaturalUserData | 

try {
    $result = $api_instance->kycOperatorPutOperatorValidation($validation_id, $operator_result);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling KycOperatorApi->kycOperatorPutOperatorValidation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **validation_id** | **int**|  |
 **operator_result** | [**\MarketPayAdmin\Model\TKycNaturalUserData**](../Model/TKycNaturalUserData.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TKycNaturalUserData**](../Model/TKycNaturalUserData.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

