# MarketPayAdmin\TpgAplazameConfigsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tpgAplazameConfigsGet**](TpgAplazameConfigsApi.md#tpgAplazameConfigsGet) | **GET** /v2.01/core/TpgAplazameConfigs/{id} | 
[**tpgAplazameConfigsPost**](TpgAplazameConfigsApi.md#tpgAplazameConfigsPost) | **POST** /v2.01/core/TpgAplazameConfigs | 


# **tpgAplazameConfigsGet**
> \MarketPayAdmin\Model\TpgAplazameConfigResponse tpgAplazameConfigsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgAplazameConfigsApi();
$id = 789; // int | 

try {
    $result = $api_instance->tpgAplazameConfigsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgAplazameConfigsApi->tpgAplazameConfigsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TpgAplazameConfigResponse**](../Model/TpgAplazameConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tpgAplazameConfigsPost**
> \MarketPayAdmin\Model\TpgAplazameConfigResponse tpgAplazameConfigsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgAplazameConfigsApi();
$request = new \MarketPayAdmin\Model\TpgAplazameConfigPost(); // \MarketPayAdmin\Model\TpgAplazameConfigPost | 

try {
    $result = $api_instance->tpgAplazameConfigsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgAplazameConfigsApi->tpgAplazameConfigsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TpgAplazameConfigPost**](../Model/TpgAplazameConfigPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TpgAplazameConfigResponse**](../Model/TpgAplazameConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

