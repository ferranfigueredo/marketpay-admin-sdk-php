# MarketPayAdmin\ShipmentMethodsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**shipmentMethodsSeurGet**](ShipmentMethodsApi.md#shipmentMethodsSeurGet) | **GET** /v2.01/core/ShipmentMethods/seur/{id} | 
[**shipmentMethodsSeurPost**](ShipmentMethodsApi.md#shipmentMethodsSeurPost) | **POST** /v2.01/core/ShipmentMethods/seur | 


# **shipmentMethodsSeurGet**
> \MarketPayAdmin\Model\ShipmentMethodSeurResponse shipmentMethodsSeurGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\ShipmentMethodsApi();
$id = 789; // int | 

try {
    $result = $api_instance->shipmentMethodsSeurGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentMethodsApi->shipmentMethodsSeurGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\ShipmentMethodSeurResponse**](../Model/ShipmentMethodSeurResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **shipmentMethodsSeurPost**
> \MarketPayAdmin\Model\ShipmentMethodSeurResponse shipmentMethodsSeurPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\ShipmentMethodsApi();
$request = new \MarketPayAdmin\Model\ShipmentMethodSeurPost(); // \MarketPayAdmin\Model\ShipmentMethodSeurPost | 

try {
    $result = $api_instance->shipmentMethodsSeurPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentMethodsApi->shipmentMethodsSeurPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\ShipmentMethodSeurPost**](../Model/ShipmentMethodSeurPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ShipmentMethodSeurResponse**](../Model/ShipmentMethodSeurResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

