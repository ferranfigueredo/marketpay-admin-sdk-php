# MarketPayAdmin\BillingApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**billingCancelBillingBatchPackage**](BillingApi.md#billingCancelBillingBatchPackage) | **POST** /v2.01/core/Billing/Packages/{PackageId}/cancel | 
[**billingCancelBillingBatchTransfer**](BillingApi.md#billingCancelBillingBatchTransfer) | **POST** /v2.01/core/Billing/payouts/{PayoutId}/cancel | 
[**billingCreateBillingBatchPayment**](BillingApi.md#billingCreateBillingBatchPayment) | **POST** /v2.01/core/Billing/Batch | 
[**billingGetBatch**](BillingApi.md#billingGetBatch) | **GET** /v2.01/core/Billing/Batch/{BatchId} | 
[**billingGetBillingFile**](BillingApi.md#billingGetBillingFile) | **GET** /v2.01/core/Billing/Packages/{PackageId}/file | 
[**billingPrepareBillingBatch**](BillingApi.md#billingPrepareBillingBatch) | **POST** /v2.01/core/Billing/Batch/{BatchId}/complete | 


# **billingCancelBillingBatchPackage**
> \MarketPayAdmin\Model\BillingPackageCancelResponse billingCancelBillingBatchPackage($package_id, $cancel_request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\BillingApi();
$package_id = 789; // int | 
$cancel_request = new \MarketPayAdmin\Model\BillingPackageCancelRequest(); // \MarketPayAdmin\Model\BillingPackageCancelRequest | 

try {
    $result = $api_instance->billingCancelBillingBatchPackage($package_id, $cancel_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingApi->billingCancelBillingBatchPackage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **package_id** | **int**|  |
 **cancel_request** | [**\MarketPayAdmin\Model\BillingPackageCancelRequest**](../Model/BillingPackageCancelRequest.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\BillingPackageCancelResponse**](../Model/BillingPackageCancelResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **billingCancelBillingBatchTransfer**
> \MarketPayAdmin\Model\BillingTransferCancelResponse billingCancelBillingBatchTransfer($payout_id, $cancel_request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\BillingApi();
$payout_id = 789; // int | 
$cancel_request = new \MarketPayAdmin\Model\BillingTransferCancelRequest(); // \MarketPayAdmin\Model\BillingTransferCancelRequest | 

try {
    $result = $api_instance->billingCancelBillingBatchTransfer($payout_id, $cancel_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingApi->billingCancelBillingBatchTransfer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payout_id** | **int**|  |
 **cancel_request** | [**\MarketPayAdmin\Model\BillingTransferCancelRequest**](../Model/BillingTransferCancelRequest.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\BillingTransferCancelResponse**](../Model/BillingTransferCancelResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **billingCreateBillingBatchPayment**
> \MarketPayAdmin\Model\CreateBillingBatchResponse billingCreateBillingBatchPayment($billing_batch_payment_post)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\BillingApi();
$billing_batch_payment_post = new \MarketPayAdmin\Model\CreateBillingBatchPaymentPost(); // \MarketPayAdmin\Model\CreateBillingBatchPaymentPost | 

try {
    $result = $api_instance->billingCreateBillingBatchPayment($billing_batch_payment_post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingApi->billingCreateBillingBatchPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **billing_batch_payment_post** | [**\MarketPayAdmin\Model\CreateBillingBatchPaymentPost**](../Model/CreateBillingBatchPaymentPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\CreateBillingBatchResponse**](../Model/CreateBillingBatchResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **billingGetBatch**
> \MarketPayAdmin\Model\BillingBatchPaymentStatusResponse billingGetBatch($batch_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\BillingApi();
$batch_id = 789; // int | 

try {
    $result = $api_instance->billingGetBatch($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingApi->billingGetBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\BillingBatchPaymentStatusResponse**](../Model/BillingBatchPaymentStatusResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **billingGetBillingFile**
> \SplFileObject billingGetBillingFile($package_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\BillingApi();
$package_id = 789; // int | 

try {
    $result = $api_instance->billingGetBillingFile($package_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingApi->billingGetBillingFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **package_id** | **int**|  |

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **billingPrepareBillingBatch**
> \MarketPayAdmin\Model\PrepareBillingBatchResponse billingPrepareBillingBatch($batch_id, $prepare_billing_batch_request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\BillingApi();
$batch_id = 789; // int | 
$prepare_billing_batch_request = new \MarketPayAdmin\Model\PrepareBillingBatchRequest(); // \MarketPayAdmin\Model\PrepareBillingBatchRequest | 

try {
    $result = $api_instance->billingPrepareBillingBatch($batch_id, $prepare_billing_batch_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BillingApi->billingPrepareBillingBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **int**|  |
 **prepare_billing_batch_request** | [**\MarketPayAdmin\Model\PrepareBillingBatchRequest**](../Model/PrepareBillingBatchRequest.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PrepareBillingBatchResponse**](../Model/PrepareBillingBatchResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

