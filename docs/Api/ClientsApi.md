# MarketPayAdmin\ClientsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clientsGet**](ClientsApi.md#clientsGet) | **GET** /v2.01/core/Clients/{id} | 
[**clientsGetList**](ClientsApi.md#clientsGetList) | **GET** /v2.01/core/Clients | 
[**clientsPost**](ClientsApi.md#clientsPost) | **POST** /v2.01/core/Clients | 


# **clientsGet**
> \MarketPayAdmin\Model\ClientResponse clientsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\ClientsApi();
$id = 789; // int | 

try {
    $result = $api_instance->clientsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientsApi->clientsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\ClientResponse**](../Model/ClientResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **clientsGetList**
> \MarketPayAdmin\Model\ResponseListClientResponse clientsGetList($page, $per_page)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\ClientsApi();
$page = 56; // int | 
$per_page = 56; // int | 

try {
    $result = $api_instance->clientsGetList($page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientsApi->clientsGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional]
 **per_page** | **int**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListClientResponse**](../Model/ResponseListClientResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **clientsPost**
> \MarketPayAdmin\Model\ClientResponse clientsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\ClientsApi();
$request = new \MarketPayAdmin\Model\ClientPost(); // \MarketPayAdmin\Model\ClientPost | 

try {
    $result = $api_instance->clientsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ClientsApi->clientsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\ClientPost**](../Model/ClientPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ClientResponse**](../Model/ClientResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

