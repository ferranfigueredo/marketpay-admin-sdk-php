# MarketPayAdmin\CardsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cardsGetClientList**](CardsApi.md#cardsGetClientList) | **GET** /v2.01/core/Cards | Clients List of Card


# **cardsGetClientList**
> \MarketPayAdmin\Model\ResponseListCardResponse cardsGetClientList($client_id, $page, $per_page)

Clients List of Card

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\CardsApi();
$client_id = 789; // int | The Id of a card
$page = 56; // int | The page
$per_page = 56; // int | Items per page

try {
    $result = $api_instance->cardsGetClientList($client_id, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CardsApi->cardsGetClientList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **int**| The Id of a card | [optional]
 **page** | **int**| The page | [optional]
 **per_page** | **int**| Items per page | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListCardResponse**](../Model/ResponseListCardResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

