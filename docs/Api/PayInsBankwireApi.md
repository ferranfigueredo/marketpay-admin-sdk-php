# MarketPayAdmin\PayInsBankwireApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payInsBankwireGetPaymentList**](PayInsBankwireApi.md#payInsBankwireGetPaymentList) | **GET** /v2.01/core/PayInsBankwire/payments | 
[**payInsBankwireGetRefundsList**](PayInsBankwireApi.md#payInsBankwireGetRefundsList) | **GET** /v2.01/core/PayInsBankwire/refunds | 
[**payInsBankwirePayInBankwireCancellation**](PayInsBankwireApi.md#payInsBankwirePayInBankwireCancellation) | **POST** /v2.01/core/PayInsBankwire/references/{WireReference}/cancellation | 
[**payInsBankwirePayInBankwireConfirmation**](PayInsBankwireApi.md#payInsBankwirePayInBankwireConfirmation) | **POST** /v2.01/core/PayInsBankwire/references/{WireReference}/confirmation | 
[**payInsBankwirePayInBankwireGetByReference**](PayInsBankwireApi.md#payInsBankwirePayInBankwireGetByReference) | **POST** /v2.01/core/PayInsBankwire/references/{WireReference} | 
[**payInsBankwirePayInBankwireRefundCancellation**](PayInsBankwireApi.md#payInsBankwirePayInBankwireRefundCancellation) | **POST** /v2.01/core/PayInsBankwire/refunds/{PayInId}/cancellation | 
[**payInsBankwirePayInBankwireRefundConfirmation**](PayInsBankwireApi.md#payInsBankwirePayInBankwireRefundConfirmation) | **POST** /v2.01/core/PayInsBankwire/refunds/{PayInId}/confirmation | 


# **payInsBankwireGetPaymentList**
> \MarketPayAdmin\Model\ResponseListPayInBankwireResponse payInsBankwireGetPaymentList($page, $per_page, $before_date, $after_date, $sort)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$page = 56; // int | 
$per_page = 56; // int | 
$before_date = 789; // int | 
$after_date = 789; // int | 
$sort = "sort_example"; // string | 

try {
    $result = $api_instance->payInsBankwireGetPaymentList($page, $per_page, $before_date, $after_date, $sort);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwireGetPaymentList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional]
 **per_page** | **int**|  | [optional]
 **before_date** | **int**|  | [optional]
 **after_date** | **int**|  | [optional]
 **sort** | **string**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListPayInBankwireResponse**](../Model/ResponseListPayInBankwireResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payInsBankwireGetRefundsList**
> \MarketPayAdmin\Model\ResponseListPayInBankwireRefundResponse payInsBankwireGetRefundsList($page, $per_page, $before_date, $after_date, $sort)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$page = 56; // int | 
$per_page = 56; // int | 
$before_date = 789; // int | 
$after_date = 789; // int | 
$sort = "sort_example"; // string | 

try {
    $result = $api_instance->payInsBankwireGetRefundsList($page, $per_page, $before_date, $after_date, $sort);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwireGetRefundsList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional]
 **per_page** | **int**|  | [optional]
 **before_date** | **int**|  | [optional]
 **after_date** | **int**|  | [optional]
 **sort** | **string**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListPayInBankwireRefundResponse**](../Model/ResponseListPayInBankwireRefundResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payInsBankwirePayInBankwireCancellation**
> \MarketPayAdmin\Model\PayInBankwireCancellationResponse payInsBankwirePayInBankwireCancellation($wire_reference, $bankwire_payment_cancellation)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$wire_reference = "wire_reference_example"; // string | 
$bankwire_payment_cancellation = new \MarketPayAdmin\Model\PayInBankwireCancellationPost(); // \MarketPayAdmin\Model\PayInBankwireCancellationPost | 

try {
    $result = $api_instance->payInsBankwirePayInBankwireCancellation($wire_reference, $bankwire_payment_cancellation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwirePayInBankwireCancellation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wire_reference** | **string**|  |
 **bankwire_payment_cancellation** | [**\MarketPayAdmin\Model\PayInBankwireCancellationPost**](../Model/PayInBankwireCancellationPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayInBankwireCancellationResponse**](../Model/PayInBankwireCancellationResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payInsBankwirePayInBankwireConfirmation**
> \MarketPayAdmin\Model\PayInBankwireConfirmationResponse payInsBankwirePayInBankwireConfirmation($wire_reference, $bankwire_payment_confirmation)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$wire_reference = "wire_reference_example"; // string | 
$bankwire_payment_confirmation = new \MarketPayAdmin\Model\PayInBankwireConfirmationPost(); // \MarketPayAdmin\Model\PayInBankwireConfirmationPost | 

try {
    $result = $api_instance->payInsBankwirePayInBankwireConfirmation($wire_reference, $bankwire_payment_confirmation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwirePayInBankwireConfirmation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wire_reference** | **string**|  |
 **bankwire_payment_confirmation** | [**\MarketPayAdmin\Model\PayInBankwireConfirmationPost**](../Model/PayInBankwireConfirmationPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayInBankwireConfirmationResponse**](../Model/PayInBankwireConfirmationResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payInsBankwirePayInBankwireGetByReference**
> \MarketPayAdmin\Model\PayInBankwireResponse payInsBankwirePayInBankwireGetByReference($wire_reference)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$wire_reference = "wire_reference_example"; // string | 

try {
    $result = $api_instance->payInsBankwirePayInBankwireGetByReference($wire_reference);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwirePayInBankwireGetByReference: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wire_reference** | **string**|  |

### Return type

[**\MarketPayAdmin\Model\PayInBankwireResponse**](../Model/PayInBankwireResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payInsBankwirePayInBankwireRefundCancellation**
> \MarketPayAdmin\Model\PayInBankwireRefundResponse payInsBankwirePayInBankwireRefundCancellation($pay_in_id, $bankwire_refund_cancellation)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$pay_in_id = 789; // int | 
$bankwire_refund_cancellation = new \MarketPayAdmin\Model\PayInBankwireRefundCancellationPost(); // \MarketPayAdmin\Model\PayInBankwireRefundCancellationPost | 

try {
    $result = $api_instance->payInsBankwirePayInBankwireRefundCancellation($pay_in_id, $bankwire_refund_cancellation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwirePayInBankwireRefundCancellation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pay_in_id** | **int**|  |
 **bankwire_refund_cancellation** | [**\MarketPayAdmin\Model\PayInBankwireRefundCancellationPost**](../Model/PayInBankwireRefundCancellationPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayInBankwireRefundResponse**](../Model/PayInBankwireRefundResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payInsBankwirePayInBankwireRefundConfirmation**
> \MarketPayAdmin\Model\PayInBankwireRefundResponse payInsBankwirePayInBankwireRefundConfirmation($pay_in_id, $bankwire_refund_confirmation)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayInsBankwireApi();
$pay_in_id = 789; // int | 
$bankwire_refund_confirmation = new \MarketPayAdmin\Model\PayInBankwireRefundConfirmationPost(); // \MarketPayAdmin\Model\PayInBankwireRefundConfirmationPost | 

try {
    $result = $api_instance->payInsBankwirePayInBankwireRefundConfirmation($pay_in_id, $bankwire_refund_confirmation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayInsBankwireApi->payInsBankwirePayInBankwireRefundConfirmation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pay_in_id** | **int**|  |
 **bankwire_refund_confirmation** | [**\MarketPayAdmin\Model\PayInBankwireRefundConfirmationPost**](../Model/PayInBankwireRefundConfirmationPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayInBankwireRefundResponse**](../Model/PayInBankwireRefundResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

