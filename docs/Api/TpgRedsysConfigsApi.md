# MarketPayAdmin\TpgRedsysConfigsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tpgRedsysConfigsGet**](TpgRedsysConfigsApi.md#tpgRedsysConfigsGet) | **GET** /v2.01/core/TpgRedsysConfigs/{id} | 
[**tpgRedsysConfigsPost**](TpgRedsysConfigsApi.md#tpgRedsysConfigsPost) | **POST** /v2.01/core/TpgRedsysConfigs | 


# **tpgRedsysConfigsGet**
> \MarketPayAdmin\Model\TpgRedsysConfigResponse tpgRedsysConfigsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgRedsysConfigsApi();
$id = 789; // int | 

try {
    $result = $api_instance->tpgRedsysConfigsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgRedsysConfigsApi->tpgRedsysConfigsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TpgRedsysConfigResponse**](../Model/TpgRedsysConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tpgRedsysConfigsPost**
> \MarketPayAdmin\Model\TpgRedsysConfigResponse tpgRedsysConfigsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgRedsysConfigsApi();
$request = new \MarketPayAdmin\Model\TpgRedsysConfigPost(); // \MarketPayAdmin\Model\TpgRedsysConfigPost | 

try {
    $result = $api_instance->tpgRedsysConfigsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgRedsysConfigsApi->tpgRedsysConfigsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TpgRedsysConfigPost**](../Model/TpgRedsysConfigPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TpgRedsysConfigResponse**](../Model/TpgRedsysConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

