# MarketPayAdmin\PartnersApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**partnersGet**](PartnersApi.md#partnersGet) | **GET** /v2.01/core/Partners/{id} | 
[**partnersGetList**](PartnersApi.md#partnersGetList) | **GET** /v2.01/core/Partners | 
[**partnersPost**](PartnersApi.md#partnersPost) | **POST** /v2.01/core/Partners | 
[**partnersPostRole**](PartnersApi.md#partnersPostRole) | **POST** /v2.01/core/Partners/{Id}/clients/{ClientId}/roles | 


# **partnersGet**
> \MarketPayAdmin\Model\PartnerResponse partnersGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PartnersApi();
$id = 789; // int | 

try {
    $result = $api_instance->partnersGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnersApi->partnersGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\PartnerResponse**](../Model/PartnerResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnersGetList**
> \MarketPayAdmin\Model\ResponseListPartnerResponse partnersGetList($page, $per_page)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PartnersApi();
$page = 56; // int | 
$per_page = 56; // int | 

try {
    $result = $api_instance->partnersGetList($page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnersApi->partnersGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional]
 **per_page** | **int**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListPartnerResponse**](../Model/ResponseListPartnerResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnersPost**
> \MarketPayAdmin\Model\PartnerResponse partnersPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PartnersApi();
$request = new \MarketPayAdmin\Model\PartnerPost(); // \MarketPayAdmin\Model\PartnerPost | 

try {
    $result = $api_instance->partnersPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnersApi->partnersPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\PartnerPost**](../Model/PartnerPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PartnerResponse**](../Model/PartnerResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **partnersPostRole**
> \MarketPayAdmin\Model\PartnerResponse partnersPostRole($id, $client_id, $request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PartnersApi();
$id = "id_example"; // string | 
$client_id = "client_id_example"; // string | 
$request = new \MarketPayAdmin\Model\PartnerRoleByClientPost(); // \MarketPayAdmin\Model\PartnerRoleByClientPost | 

try {
    $result = $api_instance->partnersPostRole($id, $client_id, $request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PartnersApi->partnersPostRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **client_id** | **string**|  |
 **request** | [**\MarketPayAdmin\Model\PartnerRoleByClientPost**](../Model/PartnerRoleByClientPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PartnerResponse**](../Model/PartnerResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

