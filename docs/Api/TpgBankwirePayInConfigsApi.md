# MarketPayAdmin\TpgBankwirePayInConfigsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tpgBankwirePayInConfigsGet**](TpgBankwirePayInConfigsApi.md#tpgBankwirePayInConfigsGet) | **GET** /v2.01/core/TpgBankwirePayInConfigs/{id} | 
[**tpgBankwirePayInConfigsPost**](TpgBankwirePayInConfigsApi.md#tpgBankwirePayInConfigsPost) | **POST** /v2.01/core/TpgBankwirePayInConfigs | 


# **tpgBankwirePayInConfigsGet**
> \MarketPayAdmin\Model\TpgBankwirePayInConfigResponse tpgBankwirePayInConfigsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgBankwirePayInConfigsApi();
$id = 789; // int | 

try {
    $result = $api_instance->tpgBankwirePayInConfigsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgBankwirePayInConfigsApi->tpgBankwirePayInConfigsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TpgBankwirePayInConfigResponse**](../Model/TpgBankwirePayInConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tpgBankwirePayInConfigsPost**
> \MarketPayAdmin\Model\TpgBankwirePayInConfigResponse tpgBankwirePayInConfigsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgBankwirePayInConfigsApi();
$request = new \MarketPayAdmin\Model\TpgBankwirePayInConfigPost(); // \MarketPayAdmin\Model\TpgBankwirePayInConfigPost | 

try {
    $result = $api_instance->tpgBankwirePayInConfigsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgBankwirePayInConfigsApi->tpgBankwirePayInConfigsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TpgBankwirePayInConfigPost**](../Model/TpgBankwirePayInConfigPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TpgBankwirePayInConfigResponse**](../Model/TpgBankwirePayInConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

