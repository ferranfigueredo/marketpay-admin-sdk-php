# MarketPayAdmin\TpgBankwirePayOutConfigsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tpgBankwirePayOutConfigsGet**](TpgBankwirePayOutConfigsApi.md#tpgBankwirePayOutConfigsGet) | **GET** /v2.01/core/TpgBankwirePayOutConfigs/{id} | 
[**tpgBankwirePayOutConfigsPost**](TpgBankwirePayOutConfigsApi.md#tpgBankwirePayOutConfigsPost) | **POST** /v2.01/core/TpgBankwirePayOutConfigs | 


# **tpgBankwirePayOutConfigsGet**
> \MarketPayAdmin\Model\TpgBankwirePayOutConfigResponse tpgBankwirePayOutConfigsGet($id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgBankwirePayOutConfigsApi();
$id = 789; // int | 

try {
    $result = $api_instance->tpgBankwirePayOutConfigsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgBankwirePayOutConfigsApi->tpgBankwirePayOutConfigsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\TpgBankwirePayOutConfigResponse**](../Model/TpgBankwirePayOutConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tpgBankwirePayOutConfigsPost**
> \MarketPayAdmin\Model\TpgBankwirePayOutConfigResponse tpgBankwirePayOutConfigsPost($request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\TpgBankwirePayOutConfigsApi();
$request = new \MarketPayAdmin\Model\TpgBankwirePayOutConfigPost(); // \MarketPayAdmin\Model\TpgBankwirePayOutConfigPost | 

try {
    $result = $api_instance->tpgBankwirePayOutConfigsPost($request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TpgBankwirePayOutConfigsApi->tpgBankwirePayOutConfigsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**\MarketPayAdmin\Model\TpgBankwirePayOutConfigPost**](../Model/TpgBankwirePayOutConfigPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\TpgBankwirePayOutConfigResponse**](../Model/TpgBankwirePayOutConfigResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

