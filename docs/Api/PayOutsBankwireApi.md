# MarketPayAdmin\PayOutsBankwireApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payOutsBankwireGetList**](PayOutsBankwireApi.md#payOutsBankwireGetList) | **GET** /v2.01/core/PayOutsBankwire/payments | 
[**payOutsBankwirePayOutBankwireCancellation**](PayOutsBankwireApi.md#payOutsBankwirePayOutBankwireCancellation) | **POST** /v2.01/core/PayOutsBankwire/payments/{PayOutId}/cancellation | 
[**payOutsBankwirePayOutBankwireConfirmation**](PayOutsBankwireApi.md#payOutsBankwirePayOutBankwireConfirmation) | **POST** /v2.01/core/PayOutsBankwire/payments/{PayOutId}/confirmation | 


# **payOutsBankwireGetList**
> \MarketPayAdmin\Model\ResponseListPayOutBankwireCoreResponse payOutsBankwireGetList($client_id, $page, $per_page)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBankwireApi();
$client_id = 789; // int | 
$page = 56; // int | 
$per_page = 56; // int | 

try {
    $result = $api_instance->payOutsBankwireGetList($client_id, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBankwireApi->payOutsBankwireGetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **int**|  | [optional]
 **page** | **int**|  | [optional]
 **per_page** | **int**|  | [optional]

### Return type

[**\MarketPayAdmin\Model\ResponseListPayOutBankwireCoreResponse**](../Model/ResponseListPayOutBankwireCoreResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBankwirePayOutBankwireCancellation**
> \MarketPayAdmin\Model\PayOutBankwireCancellationResponse payOutsBankwirePayOutBankwireCancellation($pay_out_id, $bankwire_payment_cancellation)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBankwireApi();
$pay_out_id = 789; // int | 
$bankwire_payment_cancellation = new \MarketPayAdmin\Model\PayOutBankwireCancellationPost(); // \MarketPayAdmin\Model\PayOutBankwireCancellationPost | 

try {
    $result = $api_instance->payOutsBankwirePayOutBankwireCancellation($pay_out_id, $bankwire_payment_cancellation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBankwireApi->payOutsBankwirePayOutBankwireCancellation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pay_out_id** | **int**|  |
 **bankwire_payment_cancellation** | [**\MarketPayAdmin\Model\PayOutBankwireCancellationPost**](../Model/PayOutBankwireCancellationPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayOutBankwireCancellationResponse**](../Model/PayOutBankwireCancellationResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBankwirePayOutBankwireConfirmation**
> \MarketPayAdmin\Model\PayOutBankwireConfirmationResponse payOutsBankwirePayOutBankwireConfirmation($pay_out_id, $bankwire_payment_confirmation)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBankwireApi();
$pay_out_id = 789; // int | 
$bankwire_payment_confirmation = new \MarketPayAdmin\Model\PayOutBankwireConfirmationPost(); // \MarketPayAdmin\Model\PayOutBankwireConfirmationPost | 

try {
    $result = $api_instance->payOutsBankwirePayOutBankwireConfirmation($pay_out_id, $bankwire_payment_confirmation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBankwireApi->payOutsBankwirePayOutBankwireConfirmation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pay_out_id** | **int**|  |
 **bankwire_payment_confirmation** | [**\MarketPayAdmin\Model\PayOutBankwireConfirmationPost**](../Model/PayOutBankwireConfirmationPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayOutBankwireConfirmationResponse**](../Model/PayOutBankwireConfirmationResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

