# MarketPayAdmin\PayOutsBatchApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payOutsBatchAddTransfersToBatchTransfer**](PayOutsBatchApi.md#payOutsBatchAddTransfersToBatchTransfer) | **POST** /v2.01/core/PayOutsBatch/{BatchId}/transfers | 
[**payOutsBatchCancelTransferBatchTransfer**](PayOutsBatchApi.md#payOutsBatchCancelTransferBatchTransfer) | **POST** /v2.01/core/PayOutsBatch/payouts/{PayoutId}/cancel | 
[**payOutsBatchCancelTrasferBatchPackage**](PayOutsBatchApi.md#payOutsBatchCancelTrasferBatchPackage) | **POST** /v2.01/core/PayOutsBatch/packages/{PackageId}/cancel | 
[**payOutsBatchCreateBatchPayment**](PayOutsBatchApi.md#payOutsBatchCreateBatchPayment) | **POST** /v2.01/core/PayOutsBatch | 
[**payOutsBatchGetBatch**](PayOutsBatchApi.md#payOutsBatchGetBatch) | **GET** /v2.01/core/PayOutsBatch/{BatchId} | 
[**payOutsBatchGetFile**](PayOutsBatchApi.md#payOutsBatchGetFile) | **GET** /v2.01/core/PayOutsBatch/packages/{PackageId}/file | 
[**payOutsBatchPrepareBatch**](PayOutsBatchApi.md#payOutsBatchPrepareBatch) | **POST** /v2.01/core/PayOutsBatch/{BatchId}/complete | 


# **payOutsBatchAddTransfersToBatchTransfer**
> \MarketPayAdmin\Model\AddTransfersToBatchResponse payOutsBatchAddTransfersToBatchTransfer($batch_id, $add_transfers_to_batch_post)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$batch_id = 789; // int | 
$add_transfers_to_batch_post = new \MarketPayAdmin\Model\AddTransfersToBatchPost(); // \MarketPayAdmin\Model\AddTransfersToBatchPost | 

try {
    $result = $api_instance->payOutsBatchAddTransfersToBatchTransfer($batch_id, $add_transfers_to_batch_post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchAddTransfersToBatchTransfer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **int**|  |
 **add_transfers_to_batch_post** | [**\MarketPayAdmin\Model\AddTransfersToBatchPost**](../Model/AddTransfersToBatchPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\AddTransfersToBatchResponse**](../Model/AddTransfersToBatchResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBatchCancelTransferBatchTransfer**
> \MarketPayAdmin\Model\PayoutCancelResponse payOutsBatchCancelTransferBatchTransfer($payout_id, $cancel_request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$payout_id = 789; // int | 
$cancel_request = new \MarketPayAdmin\Model\PayOutCancelRequest(); // \MarketPayAdmin\Model\PayOutCancelRequest | 

try {
    $result = $api_instance->payOutsBatchCancelTransferBatchTransfer($payout_id, $cancel_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchCancelTransferBatchTransfer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payout_id** | **int**|  |
 **cancel_request** | [**\MarketPayAdmin\Model\PayOutCancelRequest**](../Model/PayOutCancelRequest.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PayoutCancelResponse**](../Model/PayoutCancelResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBatchCancelTrasferBatchPackage**
> \MarketPayAdmin\Model\PackageCancelResponse payOutsBatchCancelTrasferBatchPackage($package_id, $cancel_request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$package_id = 789; // int | 
$cancel_request = new \MarketPayAdmin\Model\PackageCancelRequest(); // \MarketPayAdmin\Model\PackageCancelRequest | 

try {
    $result = $api_instance->payOutsBatchCancelTrasferBatchPackage($package_id, $cancel_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchCancelTrasferBatchPackage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **package_id** | **int**|  |
 **cancel_request** | [**\MarketPayAdmin\Model\PackageCancelRequest**](../Model/PackageCancelRequest.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PackageCancelResponse**](../Model/PackageCancelResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBatchCreateBatchPayment**
> \MarketPayAdmin\Model\CreateTransferBatchResponse payOutsBatchCreateBatchPayment($batch_payment_post)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$batch_payment_post = new \MarketPayAdmin\Model\CreateBatchPaymentPost(); // \MarketPayAdmin\Model\CreateBatchPaymentPost | 

try {
    $result = $api_instance->payOutsBatchCreateBatchPayment($batch_payment_post);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchCreateBatchPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_payment_post** | [**\MarketPayAdmin\Model\CreateBatchPaymentPost**](../Model/CreateBatchPaymentPost.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\CreateTransferBatchResponse**](../Model/CreateTransferBatchResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBatchGetBatch**
> \MarketPayAdmin\Model\BatchPaymentStatusResponse payOutsBatchGetBatch($batch_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$batch_id = 789; // int | 

try {
    $result = $api_instance->payOutsBatchGetBatch($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchGetBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **int**|  |

### Return type

[**\MarketPayAdmin\Model\BatchPaymentStatusResponse**](../Model/BatchPaymentStatusResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBatchGetFile**
> \SplFileObject payOutsBatchGetFile($package_id)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$package_id = 789; // int | 

try {
    $result = $api_instance->payOutsBatchGetFile($package_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchGetFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **package_id** | **int**|  |

### Return type

[**\SplFileObject**](../Model/\SplFileObject.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **payOutsBatchPrepareBatch**
> \MarketPayAdmin\Model\PrepareTransferBatchResponse payOutsBatchPrepareBatch($batch_id, $prepare_transfer_batch_request)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
MarketPayAdmin\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new MarketPayAdmin\Api\PayOutsBatchApi();
$batch_id = 789; // int | 
$prepare_transfer_batch_request = new \MarketPayAdmin\Model\PrepareTransferBatchRequest(); // \MarketPayAdmin\Model\PrepareTransferBatchRequest | 

try {
    $result = $api_instance->payOutsBatchPrepareBatch($batch_id, $prepare_transfer_batch_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayOutsBatchApi->payOutsBatchPrepareBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **int**|  |
 **prepare_transfer_batch_request** | [**\MarketPayAdmin\Model\PrepareTransferBatchRequest**](../Model/PrepareTransferBatchRequest.md)|  | [optional]

### Return type

[**\MarketPayAdmin\Model\PrepareTransferBatchResponse**](../Model/PrepareTransferBatchResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

